from twisted.internet import defer

class Game(object):

	def __init__(self, client, gameid):
		self.client = client
		self.gameid = gameid

	def join(self, teamid):
		return self.client.callWithAuth("game.join", dict(gameid=self.gameid,
			teamid=teamid))

	def endTurn(self):
		return self.client.callWithAuth("game.endTurn", dict(gameid=self.gameid))

	def getTeams(self):
		return self.client.callRemote("game.getTeams", dict(gameid=self.gameid))

	def getTurn(self):
		return self.client.callRemote("game.getTurn", dict(gameid=self.gameid))

	def getDay(self):
		return self.client.callRemote("game.getDay", dict(gameid=self.gameid))
	
	@defer.inlineCallbacks
	def getMap(self):
		mapid = yield self.client.callRemote("game.getMapid", dict(gameid=self.gameid))
		mp = yield self.client.callRemote("game.getMap", dict(mapid=mapid))
		t = yield self.client.callRemote("game.getTileset", dict(tileset=mp["tileset"]))
		defer.returnValue((mapid, mp, t))

	def getEntityType(self, t):
		return self.client.callRemote("game.getEntityType", dict(entity=t))

	def sendCommand(self, entityid, command, params):
		return self.client.callWithAuth("game.sendCommand", dict(gameid=self.gameid,
				entityid=entityid, command=command, params=params))

	def getEntities(self):
		return self.client.callRemote("game.getEntities", dict(gameid=self.gameid))

	def waitTurn(self, turn):
		return self.client.callRemote("game.waitTurn", dict(gameid=self.gameid,
			turn=turn), timeout=20)

