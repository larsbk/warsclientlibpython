from handler import HandlerBase

class PasswordHandler(HandlerBase):
	
	def authenticate(self, userid, password):
		return self.client.callRemote("authentication.password.authenticate",
			dict(userid=userid, password=password));

	def changePassword(self, newPassword):
		return self.client.callWithAuth("authentication.password.changePassword",
			dict(newPassword=newPassword))
	
	def create(self, userid, newPassword):
		return self.client.callWithAuth("authentication.password.create",
			dict(userid=userid, newPassword=newPassword))
		
