from client import TCPClient
from twisted.internet import defer
from authenticator import SingleTimeAuthenticator
from twisted.internet import reactor 
import random
from sim import game
from sim.map import Map
from sim.entity import Entity
from collections import namedtuple

def ignore(err):
    pass

class Bot(object):

    def __init__(self, team, g, params):
        self.gm = game.Game()
        self.game = g
        self.team = team
        self.game.join(team)
        d = self.game.waitTurn(team)
        d.addCallback(self.onTurnStart)
        self.mp = None
        self.params = params
        self.gameOver = defer.Deferred()

    def updateGame(self):
        @defer.inlineCallbacks
        def loadMap():
            #print "Day: %d Turn %d Team %d" % (day, tr, self.team)
            if self.mp == None:
                mpid, mp, t = yield self.game.getMap()
                Map.tilesetlist[mp["tileset"]] = t
                self.gm.loadMap(Map(mpid, mp) )
            defer.returnValue(True)

        @defer.inlineCallbacks
        def loadTeams():
            tms = yield self.game.getTeams()
            for t in tms:
                self.gm.loadTeam(t)
            #self.gm.loadTurn( (yield self.game.getTurn()) * len(self.gm.teams) + (yield self.game.getDay()))
        @defer.inlineCallbacks
        def loadEnts():
            #Get all entities
            ents = yield self.game.getEntities()
            for team in ents:
                for e in team.values():
                    if e["type"] not in Entity.types:
                        Entity.types[e["type"]] = yield self.game.getEntityType(e["type"])
                    self.gm.loadEntity(Entity(self.gm, e))
        d = []
        d.append(loadMap().addCallback(lambda x: loadEnts()))
        d.append(loadTeams())
        return defer.DeferredList(d)

        
    @defer.inlineCallbacks
    def onTurnStart(self, turn):
        """
        Called when its my turn
        """
        yield self.updateGame()
        myTeam = self.gm.teams[ self.team ]
        
        #Print board to screen 
        #self.gm.show()
        
        #Iterate through all of mine
        for ent in myTeam.getEntities().values():
            if ent.isDestroyed():
                continue

            #Find the entity which this entity will do the most damage to
            target = None
            targetDam = 0
            for otherTeam in self.gm.teams.values():
                if otherTeam == myTeam:
                    continue
                if otherTeam.isDefeated():
                    continue
                for otherEnt in otherTeam.getEntities().values():
                    if otherEnt.isDestroyed():
                        continue
                    d = sum( ent.components["Weapon"].calculateDamage(
                            ent.components["Weapon"].specs["weapons"][0],
                            [otherEnt]))
                    d2 = sum( otherEnt.components["Weapon"].calculateDamage(
                            otherEnt.components["Weapon"].specs["weapons"][0],
                            [ent]))
                    if d > targetDam:
                        if ent["fuel"] < self.params.fuelRage or d >= d2 / self.params.agro:
                            target = otherEnt["xpos"], otherEnt["ypos"]
                            targetDam = d

            if random.random() <= self.params.attAfter:
                self.attackEntity(ent, target)

            #Move towards target or random if no target was found
            directions = []
            road = ""
            if target != None:
                if target[0] > ent["xpos"]:
                        directions.append("E")
                elif target[0] < ent["xpos"]:
                        directions.append("W")
                if target[1] < ent["ypos"]:
                        directions.append("N")
                elif target[1] > ent["ypos"]:
                        directions.append("S")
                if len(directions) == 0:
                    break
                while True:
                    if len(directions) == 0:
                            break
                    dir = random.choice( directions)
                    try:
                        #Try to move entity locally
                        self.gm.all_entities[ent["entityid"]].message("move", {"direction":dir})
                        road += dir
                    except Exception as e:
                        directions.remove(dir)

            if road == "":
                road = random.choice(["N","W","E","S"])
                try:
                    yield self.game.sendCommand(ent["entityid"], "move", {"direction":road})
                except:
                    pass
            else: 
                try:
                    #Send the command to move the entire path we have walked locally
                    yield self.game.sendCommand(ent["entityid"], "move", {"direction":road})
                except Exception as e:
                    print(e)
                    pass

            #Try to attack the target
            if random.random() <= self.params.attAfter:
                self.attackEntity(ent, target)
        #End our turn
        yield self.game.endTurn()
        d = self.game.waitTurn(self.team)
        d.addCallbacks(self.onTurnStart, self.onGameOver)

    def attackEntity(self, ent, target):
        try:
            ent.message("fire", {"xpos":target[0], "ypos":target[1]})
        except:
            return
        a = self.game.sendCommand(ent["entityid"], command="fire", 
                params={"xpos":target[0], "ypos":target[1]})
        a.addCallbacks(ignore)

    def wait(self, t):
        d = defer.Deferred()
        def w():
            d.callback(True)
        reactor.callLater(t, w)
        return d

    @defer.inlineCallbacks
    def onGameOver(self, res):
        yield self.updateGame()
        #print "Gameover, %s, Team %d is defeated: %s, has alive entity: %s" % (str(res), self.team, str(self.gm.teams[self.team].isDefeated()), str(self.gm.teams[self.team].hasAliveEntity()))
        self.gameOver.callback(True)

    def won(self):
        return not self.gm.teams[self.team].isDefeated()

maps = [
        ("zig", "test"),
        ("test", "test"),
        ("test", "test2"),
        ("test", "test3")
]

@defer.inlineCallbacks
def tournament(client):
    global best_params
    while True:
        try:
            win1, win2 = 0,0
            p = []
            for param, s in zip(random.choice(best_param_list), sigma):
                p.append( random.gauss(param, s) )
            params2 = BotParams( *p )
            params1 = best_param_list[-1]
            print("{} vs\n{}".format(params1, params2))
            for mapid, entityset in maps:
                g = yield client.games.create(mapid, entityset)
                print ("Starting game", g.gameid)
                b1 = Bot(1, g, params1)
                b2 = Bot(2, g, params2)
                
                yield b1.gameOver
                yield b2.gameOver
                if b1.won():
                    win1 += 1
                if b2.won():
                    win2 += 1
            print(win1, win2)
            if win2 > win1:
                print "Found new best!", b2.params
                best_param_list.append(params2)
            else:
                print("Challenger defeated!")
        except Exception as e:
            print e


BotParams = namedtuple("BotParams", [
    "agro",
    "fuelRage",
    "attBefore",
    "attAfter"])
best_param_list = []
sigma = BotParams(0.5, 10.0, 0.2, 0.2)
try:
    f = open("best_params", "r")
    best_param_list = eval(f.read())
    f.close()
except:
    best_param_list.append(BotParams(2.0, 50, 1.0, 1.0))
def setup():
    #client = TCPClient("37.139.22.75", 2337);
    #client = TCPClient("127.0.0.1", 2337);
    client = TCPClient("wars.larsbk.me", 2337);
    a = SingleTimeAuthenticator("test_user", "correct_password")
    client.setAuthenticator(a)
    return tournament(client)
reactor.callLater(0, setup)
reactor.run()

print("Writing best params")
f = open("best_params", "w")
f.write( repr(best_param_list) )
f.close()


