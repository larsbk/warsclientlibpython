from twisted.internet import defer

class Authenticator(object):

	def __init__(self):
		self.token = None

	@defer.inlineCallbacks
	def getToken(self, conn):
		if self.token == None:
			yield self.requireLogin(conn);
		defer.returnValue(self.token)
	
	def requireLogin(self, conn):
		raise RuntimeError("Authentication required. Subclass Authenticator and implement requireLogin");
	
class SingleTimeAuthenticator(Authenticator):
	
	def __init__(self, username, password):
		super(SingleTimeAuthenticator, self).__init__()
		self.username = username
		self.password = password

	@defer.inlineCallbacks	
	def requireLogin(self, conn):
		userid = yield conn.user.getUserid(self.username)
		self.token = yield conn.authpw.authenticate(
			userid, self.password);
		self.username = None
		self.password = None
		if self.token == None:
			raise RuntimeError("Authentication failure!");
		
	
