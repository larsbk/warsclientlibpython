from handler import HandlerBase

class UserHandler(HandlerBase):
	
	def getUserid(self, username):
		return self.client.callRemote("user.getUserid", dict(username=username))
	
	def getUsername(self, userid):
		return self.client.callRemote("user.getUsername", dict(userid=userid));

	def create(self, username):
		return self.client.callRemote("user.create", dict(username=username));

	def getJoinedGames(self, userid):
		return self.client.callRemote("user.getJoinedGames", dict(userid=userid));

	def getInvitedGames(self, userid):
		return self.client.callRemote("user.getInvitedGames", dict(userid=userid));



