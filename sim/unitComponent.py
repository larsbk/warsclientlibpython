from component import Component
from warsexceptions import *
from twisted.python import log

class componentUnit(Component):
	
	def __init__(self, parent, specs=dict()):
		super(componentUnit, self).__init__(parent, specs)

	
	def newRound(self):
		self.parent.data["time"] = self.parent.specs["data"]["time"]
	
	def newDay(self):
		try:
			self.parent.subtractValue("fuel", self.parent.specs["dailyfuelusage"])
		except DataResourceError:
			self.parent.getData()["health"] = 0

	
		
