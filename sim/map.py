import json
import string
import copy
from dataObject import DataObject


def getMap(mapid):
	if(mapid not in Map.maplist):
		raise Exception("No such map: " + mapid)
	return Map.maplist[mapid]

def get_map(req):
	resp = transaction.respMessage(req, True)
	m = getMap(req["mapid"])
	if(m == None):
		return transaction.respMessage(req, False)
	resp["map"] = m.data
	return resp

def get_tileset(req):
	resp = transaction.respMessage(req, True)
	resp["tileset"] = getTileset(req["tileset"] )
	return resp

def getTileset(name):
	if(name not in Map.tilesetlist):
		raise Exception("No such tileset")
	return Map.tilesetlist[name]

def getEntityset(name):
	if name in Map.entitysetlist:
		return Map.entitysetlist[name]
	raise Exception("No such entityset!")

def get_maplist(req):
	resp = transaction.respMessage(req, True)
	resp["maps"] = Map.maplist.keys()
	return resp

class Map(DataObject):
	
	tilesetlist = dict()
	maplist = dict()
	entitysetlist = dict()
	
	def __init__(self, mapid, data):
		self.data = data
		self.data["mapid"] = mapid
		self.tileset = getTileset(self.data["tileset"])

	def getMapid(self):
		return self.data["mapid"]

	def getTileset(self):
		return self.tileset

	def getTile(self, x, y):
		if( x < 0 or y < 0 or y >= len(self.data["tiles"])):
			return None
		line = self.data["tiles"][y]
		if( x >= len(line)):
			return None
		t = line[x]
		return self.getTileset()[t]

	def getColors(self):
		out = []
		for row in self.data["tiles"]:
			line = []
			for t in row:
				line.append( {"char":" ", "fg":"grey", "bg":self.getTileset()[t]["color"], "attr":[]})
			out.append(line)
		return out


