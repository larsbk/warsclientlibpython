from component import Component
from twisted.python import log

class componentResupply(Component):

	def __init__(self, parent, specs=dict()):
		super(componentResupply, self).__init__(parent, specs)
	
	def newDay(self):
		log.msg("resupply newDay!")
		targets = self.parent.getEntitiesAtMyPos()
		for t in targets:
			if not t.isDestroyed():
				for k,v in self.specs["supplies"].items():
					try:
						t.increaseValue(k, v, top=True)	
					except DataResourceError:
						pass
	

