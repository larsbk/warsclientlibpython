from component import Component
import map
from twisted.python import log

class componentTile(Component):
	def __init__(self, parent, specs=dict()):
		super(componentTile, self).__init__(parent, specs)

	def getTile(self):
		return self.parent.map.getTileset()[ self.specs["tile"] ]


