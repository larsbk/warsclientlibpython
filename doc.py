from twisted.internet import reactor, defer
from txjason.netstring import JSONRPCClientFactory
from twisted.internet.endpoints import TCP4ClientEndpoint
from sys import argv

point = TCP4ClientEndpoint(reactor, argv[1], int(argv[2]))
conn = JSONRPCClientFactory(point)

@defer.inlineCallbacks
def printAll():
	print "API documentation for %s:%s\n==============" % (argv[1], argv[2])
	mList = yield conn.callRemote("introspection.listMethods")
	for m in sorted(mList):
		f = yield conn.callRemote("introspection.methodHelp", m)
		a = yield conn.callRemote("introspection.methodSignature", m)
		print("\n%s\n----------" % m)
		if a:
			print "### Parameters\n"
			try:
				for arg,t in a["types"].iteritems():
					if "required" in a:
						if arg in a["required"]:
							print "* %s - %s (required)" % (arg, t)
						else:
							print "* %s - %s (optional)" % (arg, t)
					else:
						print "%s - %s" % (arg, t)
			except AttributeError:
				s = ",".join(a["types"])
				print "(%s)\n" % s
		if f:
			print ("\n### Description\n%s\n" % f)
		else:
			print "\n(no description)"

	reactor.stop()

reactor.callLater(0,printAll)
reactor.run()

