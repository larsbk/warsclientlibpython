from super import SuperTest
from authenticator import SingleTimeAuthenticator
from txjason.client import JSONRPCClientError
from twisted.internet import defer

class AuthTest(SuperTest):

	def testPasswordInvalidRequest(self):
		return self.assertFailure(self.client.callRemote("authentication.plaintext"), JSONRPCClientError);
	
	def testPasswordNonexistingUser(self):
		return self.assertFailure( self.client.authpw.authenticate(
			"this_user_does_not_exist", "42"), JSONRPCClientError);
	
	@defer.inlineCallbacks
	def testPasswordWrongPW(self):
		userid = yield self.client.user.getUserid("test_user")
		ok = yield self.assertFailure( self.client.authpw.authenticate(
			userid, "wrong_password"), JSONRPCClientError)
		defer.returnValue(True)
	
	@defer.inlineCallbacks
	def testPasswordCorrectPW(self):
		userid = yield self.client.user.getUserid("test_user")
		ok = yield self.client.authpw.authenticate(
			userid, "correct_password")
		defer.returnValue(True)

	@defer.inlineCallbacks
	def testChangePassword(self):
		userid = yield self.client.user.getUserid("test_user")
		print userid
		self.assertFailure(self.client.authpw.authenticate(
			userid, "new_pw"), JSONRPCClientError)
		auth = SingleTimeAuthenticator("test_user", "correct_password");
		self.client.setAuthenticator(auth)
		print "Changing pw"
		ok1 = (yield self.client.authpw.changePassword("new_pw"))
		print "Password set"
		self.assertFailure(self.client.authpw.changePassword("new_pw"), JSONRPCClientError)
		ok2 = (yield self.client.authpw.authenticate(
			userid, "new_pw"))
		print "new pw tested"
		self.assertFailure(self.client.authpw.authenticate(
			userid, "correct_password"), JSONRPCClientError)
		
		auth = SingleTimeAuthenticator("test_user", "new_pw");
		self.client.setAuthenticator(auth)
		ok = yield self.client.authpw.changePassword("correct_password");
		defer.returnValue(ok)
