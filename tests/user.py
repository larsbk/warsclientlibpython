from super import SuperTest
from twisted.internet import defer
from txjason.client import JSONRPCClientError

class UserTest(SuperTest):
	
	def testGetValidUser(self):
		return self.client.user.getUserid("test_user");
	
	def testGetInvalidUser(self):
		return self.assertFailure(
			self.client.user.getUserid("this_user_does_not_exist"), JSONRPCClientError)
	
	@defer.inlineCallbacks
	def testGetUsername(self):
		userid = yield self.client.user.getUserid("test_user");
		username = yield self.client.user.getUsername(userid);
		self.assertTrue("test_user" == username);
		
	@defer.inlineCallbacks
	def testGetJoined(self):
		userid = yield self.client.user.getUserid("test_user");
		res = yield self.client.user.getJoinedGames(userid);
                assert len(res) > 0
