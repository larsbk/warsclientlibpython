from super import SuperTest
from twisted.internet import defer
from txjason.client import JSONRPCClientError
from authenticator import SingleTimeAuthenticator

class GamesTest(SuperTest):
	
	def setUp(self):
		super(GamesTest, self).setUp()
		a = SingleTimeAuthenticator("test_user", "correct_password")
		self.client.setAuthenticator(a)
	
	def testCreateInvalid(self):
		return self.assertFailure( self.client.games.create("no_such_map", "lol"), JSONRPCClientError)

	def testCreateGame(self):
		return self.client.games.create("test","test")
