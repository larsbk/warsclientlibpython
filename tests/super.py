from twisted.trial.unittest import TestCase
from client import TCPClient

class SuperTest(TestCase):
	def setUp(self):
		#self.client = TCPClient("37.139.22.75", 2337);
		self.client = TCPClient("127.0.0.1", 2337);
		self.timeout = 20

	def tearDown(self):
		return self.client.disconnect();
