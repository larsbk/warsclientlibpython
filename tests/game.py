from super import SuperTest
from twisted.internet import defer
from txjason.client import JSONRPCClientError
from authenticator import SingleTimeAuthenticator

class GameTest(SuperTest):

	def setUp(self):
		super(GameTest, self).setUp()
		a = SingleTimeAuthenticator("test_user", "correct_password")
		self.client.setAuthenticator(a)

        @defer.inlineCallbacks
        def testStartAtOne(self):
		game = yield self.client.games.create("test","test")
                turn = yield game.getTurn()
                assert turn == 1

	@defer.inlineCallbacks
	def testJoin(self):
		game = yield self.client.games.create("test","test")
		p1 = yield game.getTeams()
                assert p1[0]["userid"] == None
		ok = yield game.join(1)
		p2 = yield game.getTeams()
                assert p2[0]["userid"] != None
                ok2 = game.join(1)
		yield self.assertFailure( ok2, JSONRPCClientError)

        @defer.inlineCallbacks
        def testEnd(self):
		game = yield self.client.games.create("test","test")
                yield game.join(1)
                t1 = yield game.getTurn()
                yield game.endTurn()
                t2 = yield game.getTurn()
                assert t1 + 1 == t2
                
        @defer.inlineCallbacks
        def testEndDouble(self):
		game = yield self.client.games.create("test","test")
                yield game.join(1)
                yield game.endTurn()
                yield self.assertFailure( game.endTurn(), JSONRPCClientError)

        @defer.inlineCallbacks
        def testTurnWrap(self):
                game = yield self.client.games.create("test","test")
                yield game.join(1)
                yield game.join(2)
                t0 = yield game.getTurn()
                d0 = yield game.getDay()
                assert t0 == 1
                assert d0 == 1
                for i in xrange(0, 10):
                    t1 = yield game.getTurn()
                    d1 = yield game.getDay()
                    yield game.endTurn()
                    t2 = yield game.getTurn()
                    d2 = yield game.getDay()

                    assert t1 + 1 == t2
                    assert d1 == d2

                    yield game.endTurn()
                    t3 = yield game.getTurn()
                    d3 = yield game.getDay()

                    assert t3 == t0
                    assert d2 + 1 == d3
                    
        @defer.inlineCallbacks
        def testDeathByFuelLoss(self):
                game = yield self.client.games.create("test","test")
                yield game.join(1)
                yield game.join(2)
                for i in xrange(0, 100):
                    yield game.endTurn()
                    yield game.endTurn()
                #check end of game

        @defer.inlineCallbacks
        def testGetEntities(self):
                game = yield self.client.games.create("test","test")
                e = yield game.getEntities()
                assert len(e) == 2
                assert len(e[0]) == 3
                assert len(e[1]) == 3
        """
        @defer.inlineCallbacks
        def testLoadGame(self):
            import game
            d = yield game.Game(self.client, "53514a09292db54e77786d7a").getDay()
        """
        @defer.inlineCallbacks
        def testOnTurn(self):
                game = yield self.client.games.create("test","test")
                yield game.join(1)
                hasEnded = False
                def check(res):
                    assert hasEnded
                d = game.waitTurn(2)
                d.addCallback(check)
                el = yield game.getEntities()
                hasEnded = True
                yield game.endTurn()
                yield d

        @defer.inlineCallbacks
        def testCommand(self):
                game = yield self.client.games.create("test","test")
                yield game.join(1)
                yield game.join(2)
                el = yield game.getEntities()
                e1 = None
                e2 = None
                for ent in el[0].values():
                    if ent["xpos"] == 2 == ent["ypos"]:
                        e1 = ent
                    if ent["xpos"] == 2 and ent["ypos"] == 1:
                        e2 = ent

                assert e1
                assert e2
                res = yield game.sendCommand(e1["entityid"], "fire",
                        {"xpos":2, "ypos":1})
                el = yield game.getEntities()
                for ent in el[0].values():
                    #if ent["xpos"] == 2 == ent["ypos"]:
                    #    assert ent["ammo1"] < e1["ammo1"]
                    if ent["xpos"] == 2 and ent["ypos"] == 1:
                        assert ent["health"] < e2["health"]





