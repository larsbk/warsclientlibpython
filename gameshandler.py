from handler import HandlerBase
from twisted.internet import defer
from game import Game

class GamesHandler(HandlerBase):

	@defer.inlineCallbacks
	def create(self, mapid, entityset):
		g = yield self.client.callWithAuth("games.create", dict(mapid=mapid, entityset=entityset))
		defer.returnValue(Game(self.client, g))


