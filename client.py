from txjason.netstring import JSONRPCClientFactory
from twisted.internet import defer
from passwordhandler import PasswordHandler
from userhandler import UserHandler
from debughandler import DebugHandler
from gameshandler import GamesHandler

from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ClientEndpoint

class Client(object):

	def __init__(self):
		self.authpw = PasswordHandler(self)
		self.user = UserHandler(self)
		self.debug = DebugHandler(self)
		self.games = GamesHandler(self)

	def callRemote(self, method, *args, **kwargs):
		return self.conn.callRemote(method, *args, **kwargs)
	
	@defer.inlineCallbacks
	def callWithAuth(self, method, *args, **kwargs):
		token = yield self.authenticator.getToken(self)
		params = args[0]
		params["token"] = token
		ret = yield self.callRemote(method, params, **kwargs)
		defer.returnValue(ret)

	def setAuthenticator(self, authenticator):
		self.authenticator = authenticator

	def disconnect(self):
		return self.conn.disconnect()

class TCPClient(Client):
	def __init__(self, addr, port):
		super(TCPClient, self).__init__()
		point = TCP4ClientEndpoint(reactor, addr, int(port))
		self.conn = JSONRPCClientFactory(point)


